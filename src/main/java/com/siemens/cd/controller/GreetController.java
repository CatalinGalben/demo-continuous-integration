package com.siemens.cd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetController {
	
	@RequestMapping(path = "/greet/{name}", method = RequestMethod.GET)
	public String greet (@PathVariable String name, Model model) {
		String greetings = "Hello" + name;
		model.addAttribute("greet", greetings);
		
		return "greet";
	}
	@RequestMapping(path = "/")
	public String index() {
		return "index";
	}
	
	
}
